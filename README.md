# Weather App
Weather App displays the current temperature and weather of a US city.

:sunny: :cloud: :zap: :snowflake: :full_moon:

[[_TOC_]]

## Project Setup
### Install Dependencies

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

## Deploy Locally

### Initialize Project Configuration Files
```tf
terraform init
```

### Execute Actions in Terraform Plan
```tf
terraform apply
```

### Destroy Resources
```tf
terraform destroy
```

## Usage
1. Go to Weather App url
2. Enter city and state code into search bar
    - Eg:   
        - Bloomington, IL
        - Phoenix, AZ
        - Atlanta, GA
        - Dallas, TX
3. Press enter

## Tech Stack
* [Weatherbit API](https://www.weatherbit.io/api/weather-current)
* Vue JS
* GitLab CI/CD
* Terraform
* Amazon Web Services (AWS)
    * Amazon Simple Storage Service (S3)
    * Amazon Elastic Compute Cloud (EC2)
    * Virtual Private Cloud (VPC)
    * Identity and Access Management (IAM)

## Architecture and Flow
![diagram](/diagram.png)

## TODO
- [ ] Get current weather from user's geolocation
    - [ ] Set up HTTPS/SSL through AWS Certificate Manager (ACM) and Route53
- [ ] Add unit tests in project and pipeline
- [ ] Use better GitLab branching strategy with `only` job keyword
- [ ] Externalize generated CSS and JS file names into variables for terraform
