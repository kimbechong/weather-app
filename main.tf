terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-west-1"
}

resource "random_pet" "bucket_name" {
  prefix = "weather-app"
  length = 1
}

resource "aws_s3_bucket" "weather_bucket" {
  bucket        = random_pet.bucket_name.id
  force_destroy = true
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  bucket = aws_s3_bucket.weather_bucket.id
  acl    = "public-read"
}

# resource "aws_s3_bucket_object" "bucket_files" {
#   for_each = fileset("./web/", "*")
#   bucket   = aws_s3_bucket.weather_bucket.id
#   key      = each.value
#   source   = "./web/${each.value}"
# }

resource "aws_s3_bucket_object" "dist" {
  for_each = fileset("./dist/", "*")
  bucket   = aws_s3_bucket.weather_bucket.id
  key      = each.value
  source   = "./dist/${each.value}"
}

resource "aws_s3_bucket_object" "assets" {
  for_each = fileset("./dist/assets/", "*")
  bucket   = aws_s3_bucket.weather_bucket.id
  key      = each.value
  source   = "./dist/assets/${each.value}"
}


# Reference the default VPC to add security group to
resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_security_group" "allow_server" {
  name        = "allow_server"
  description = "Allow default inbound traffic"
  vpc_id      = aws_default_vpc.default.id

  ingress {
    description = "http"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "https"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["98.165.195.83/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_server"
  }
}

resource "aws_instance" "weather_web_server" {
  ami                  = "ami-0d9858aa3c6322f73"
  instance_type        = "t2.micro"
  iam_instance_profile = aws_iam_instance_profile.weather_profile.name
  key_name             = "us-west-1"
  security_groups      = ["allow_server"]
  user_data            = <<EOF
#!/bin/bash
sudo su
yum -y update && sudo yum -y upgrade
yum install httpd -y
service httpd start
aws s3api get-object --bucket "${random_pet.bucket_name.id}" --key "index.html" "index.html"
aws s3api get-object --bucket "${random_pet.bucket_name.id}" --key "cloud.ico" "cloud.ico"
aws s3api get-object --bucket "${random_pet.bucket_name.id}" --key "index.0df78667.css" "index.0df78667.css"
aws s3api get-object --bucket "${random_pet.bucket_name.id}" --key "index.33dc9215.js" "index.33dc9215.js"
cp index.html /var/www/html
cp cloud.ico /var/www/html
mkdir /var/www/html/assets
cp index.0df78667.css /var/www/html/assets
cp index.33dc9215.js /var/www/html/assets
EOF
  tags = {
    Name = "Weather Web Server"
  }

  depends_on = [
    aws_security_group.allow_server
  ]

}

resource "aws_iam_role" "weather_role" {
  name = "weather_role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Name = "weather_role"
  }
}

resource "aws_iam_instance_profile" "weather_profile" {
  name = "weather_profile"
  role = aws_iam_role.weather_role.name
}

resource "aws_iam_role_policy" "s3_access" {
  name = "s3_access"
  role = aws_iam_role.weather_role.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "s3:GetObject",
                "s3:ListBucket"
            ],
            "Resource": [
                "arn:aws:s3:::${random_pet.bucket_name.id}/*",
                "arn:aws:s3:::${random_pet.bucket_name.id}"
            ]
        },
        {
            "Effect": "Allow",
            "Action": "s3:ListAllMyBuckets",
            "Resource": "*"
        }
    ]
}
EOF
}
