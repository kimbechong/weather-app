window.addEventListener('load', () => {
    let long;
    let lat;
    let tempDesc = document.querySelector(".temperature-description");
    let tempDegree = document.querySelector(".temperature-degree");
    let locCityState = document.querySelector(".location-city-state");
    let icon = document.getElementById("icon");
    let color1;
    let color2;

    if (!navigator.geolocation) {
        //default lat/long PHX
        renderWeather(33.448376, -112.074036);
        return;
    }

    navigator.geolocation.getCurrentPosition(position => {
        long = position.coords.longitude;
        lat = position.coords.latitude;
        renderWeather(lat, long);
    });

    function renderWeather(lat, long) {
        const apiKey = "133529d561b14c558dd8e36aa312679b";
        const api = `https://api.weatherbit.io/v2.0/current?lat=${lat}&lon=${long}&units=I&key=${apiKey}`;

        fetch(api).then(response => {
            return response.json();
        }).then(data => {
            const { city_name, state_code, temp, weather } = data.data[0];

            // set DOM elements from the API
            locCityState.textContent = `${city_name}, ${state_code}`;
            tempDegree.textContent = temp;
            tempDesc.textContent = weather.description;
            icon.src = `https://www.weatherbit.io/static/img/icons/${weather.icon}.png`;

            setTempColors(temp);
            document.body.style.background = `linear-gradient(${color1}, ${color2})`;
        });
    }

    function setTempColors(temp) {
        if (temp <= 5) {
            color2 = "#000000";
            color1 = "#303030";
        } else if (temp > 5 && temp <= 14) {
            color2 = "#303030";
            color1 = "#32309d";
        } else if (temp > 14 && temp <= 23) {
            color2 = "#32309d";
            color1 = "#000084";
        } else if (temp > 23 && temp <= 32) {
            color2 = "#000084";
            color1 = "#0000fe";
        } else if (temp > 32 && temp <= 41) {
            color2 = "#0000fe";
            color1 = "#3065ff";
        } else if (temp > 41 && temp <= 50) {
            color2 = "#3065ff";
            color1 = "#9cceff";
        } else if (temp > 50 && temp <= 59) {
            color2 = "#9cceff";
            color1 = "#31cfce";
        } else if (temp > 59 && temp <= 68) {
            color2 = "#31cfce";
            color1 = "#008100";
        } else if (temp > 68 && temp <= 77) {
            color2 = "#008100";
            color1 = "#9bcf00";
        } else if (temp > 68 && temp <= 77) {
            color2 = "#9bcf00";
            color1 = "#ffcf00";
        } else if (temp > 77 && temp <= 86) {
            color2 = "#ffcf00";
            color1 = "#fb6705";
        } else {
            color2 = "#fb6705";
            color1 = "#fe0000";
        }
        console.log(color1);
        console.log(color2);
    }

});